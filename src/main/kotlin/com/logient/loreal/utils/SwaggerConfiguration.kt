package com.logient.loreal.utils

import com.google.common.base.Predicates
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.util.*

//TODO Find login + define apiInfo() properly
@Configuration
@EnableSwagger2
@Profile("dev", "local", "sit")
class SwaggerConfiguration {
    @Bean
    fun api(): Docket = Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .paths(Predicates.not(PathSelectors.regex("/error.*")))
            .build()
            .apiInfo(apiInfo())

    private fun apiInfo(): ApiInfo {
        return ApiInfo("Bierre&Co REST API",
                "API used by the Bierre&Co app frontend to communicate with the app backend.",
                "API 1.0",
                "Terms of service",
                Contact("support@logient.com", "", "support@logient.com"),
                "",
                "",
                ArrayList())
    }
}