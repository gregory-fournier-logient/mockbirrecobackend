package com.logient.loreal.utils

import java.io.File
import java.io.FileInputStream
import java.security.MessageDigest

enum class Hash constructor(private val value: String) {

    MD5("MD5"),
    SHA1("SHA1"),
    SHA256("SHA-256"),
    SHA512("SHA-512");

    fun checksum(input: File): ByteArray {
        try {
            FileInputStream(input).use { inputStream ->
                val digest = MessageDigest.getInstance(value)
                val block = ByteArray(4096)
                var length = 0

                while (length != -1) {
                    digest.update(block, 0, length)
                    length = inputStream.read(block)
                }

                return digest.digest()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ByteArray(0)
    }
}