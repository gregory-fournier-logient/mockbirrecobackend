package com.logient.loreal.utils

import java.net.URI
import java.nio.file.Path
import java.nio.file.Paths


fun String.toPath(): Path {
    return Paths.get(this).toAbsolutePath().normalize()!!
}

fun String.toUri(): URI {
    return this.toPath().toUri()
}