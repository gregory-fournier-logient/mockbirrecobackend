package com.logient.loreal.utils

import com.logient.loreal.service.exception.MyFileNotFoundException
import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.net.MalformedURLException
import java.nio.file.Path
import javax.xml.bind.DatatypeConverter


object FileUtils {
    internal fun loadFilePathAsResource(filePath: Path, fileName: String): Resource {
        try {
            val resource = UrlResource(filePath.resolve(fileName).normalize().toUri())
            return if (resource.exists()) {
                resource
            } else {
                throw MyFileNotFoundException("File not found $fileName")
            }
        } catch (ex: MalformedURLException) {
            throw MyFileNotFoundException("File not found $fileName", ex)
        }
    }

    internal fun loadFileAsResource(file: File): Resource {
        try {
            val resource = UrlResource(file.toURI())
            return if (resource.exists()) {
                resource
            } else {
                throw MyFileNotFoundException("File not found")
            }
        } catch (ex: MalformedURLException) {
            throw MyFileNotFoundException("File not found", ex)
        }
    }

    internal fun loadStreamAsResource(baos: ByteArrayOutputStream): Resource {
        val bais = ByteArrayInputStream(baos.toByteArray())
        try {
            return InputStreamResource(bais)
        } catch (ex: Exception) {
            throw MyFileNotFoundException("File not found", ex)
        } finally {
            baos.close()
            bais.close()
        }
    }
}

fun File.checksum(): String{
    return DatatypeConverter.printHexBinary(Hash.SHA512.checksum(this))
}