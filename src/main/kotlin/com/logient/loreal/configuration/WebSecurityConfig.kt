package com.logient.loreal.configuration

import com.google.common.collect.ImmutableList
import com.logient.loreal.controllers.UserController
import com.logient.loreal.security.JWTAuthenticationFilter
import com.logient.loreal.security.JWTAuthorizationFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import java.util.*


@Suppress("SpringJavaInjectionPointsAutowiringInspection")
@EnableWebSecurity
@Configuration
//Annotation absolutely mandatory to manage authority and role with SpringSecurity annotation
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class WebSecurityConfig(val userDetailsService: UserDetailsService,
                        val bCryptPasswordEncoder: BCryptPasswordEncoder) : WebSecurityConfigurerAdapter() {


    override fun configure(http: HttpSecurity) {
        //Other role management example :
        // We can add the following line before ".anyRequest().fullyAuthenticated().and()"
        //
        //.antMatchers(HttpMethod.GET, UserController.API).hasAuthority(UserModel.Role.SUPER_ADMIN.authority)
        //or
        //.antMatchers(HttpMethod.GET, UserController.API).hasRole(UserModel.Role.SUPER_ADMIN.name) (Role_ should not be in the string)

        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, LOGIN_ENDPOINT).permitAll()
                .antMatchers(HttpMethod.POST, USERS_ENDPOINT).permitAll()
                .antMatchers(HttpMethod.GET, PRODUCTS_ENDPOINT).permitAll()
                .antMatchers(HttpMethod.POST, UserController.PASSWORD_API).permitAll()
                .anyRequest().fullyAuthenticated().and()
                .addFilter(getJWTAuthenticationFilter())
                .addFilter(JWTAuthorizationFilter(authenticationManager()))
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    @Throws(Exception::class)
    override fun configure(web: WebSecurity) {
        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/**",
                "/swagger-ui.html",
                "/webjars/**")
    }

    public override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.userDetailsService(userDetailsService)?.passwordEncoder(bCryptPasswordEncoder)
    }

    /**
     * Enable/Disable Headers,Methods,etc. here.
     */
    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowedOrigins = ImmutableList.of("*")
        configuration.allowCredentials = true
        configuration.allowedMethods = Arrays.asList("OPTIONS", "GET", "POST", "PUT", "DELETE")
        configuration.maxAge = 3600
        configuration.allowedHeaders = ImmutableList.of("Authorization", "Cache-Control", "Content-Type")
        configuration.exposedHeaders = ImmutableList.of("Authorization", "session_remaining_time", "refresh_token", "new_token")
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

    /**
     * Configure custom endpoint for logging in
     */
    @Bean
    fun getJWTAuthenticationFilter(): JWTAuthenticationFilter {
        val filter = JWTAuthenticationFilter(authenticationManager())
        filter.setFilterProcessesUrl(LOGIN_ENDPOINT)
        return filter
    }

    companion object {
        const val LOGIN_ENDPOINT = "/api/login"
        const val USERS_ENDPOINT = "/api/users"
        const val PRODUCTS_ENDPOINT = "/api/products"
    }
}