package com.logient.loreal.configuration

// TODO: fail with with Postgres 11
//https://github.com/flyway/flyway/issues/2178
//@Configuration
//class FlywayConfig {
//
//    @Bean
//    @Profile("dev", "local", "sit")
//    fun cleanMigrateStrategy(): FlywayMigrationStrategy {
//        return FlywayMigrationStrategy { flyway ->
//            try {
//                flyway.validate()
//            } catch (e: FlywayException) {
//                flyway.clean()
//            }
//            flyway.migrate()
//        }
//    }
//}