package com.logient.loreal

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class LorealApplication

fun main(args: Array<String>) {
    runApplication<LorealApplication>(*args)
}
