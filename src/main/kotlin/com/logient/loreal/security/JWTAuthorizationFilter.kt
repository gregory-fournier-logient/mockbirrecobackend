package com.logient.loreal.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.TokenExpiredException
import com.auth0.jwt.interfaces.DecodedJWT
import com.logient.loreal.database.model.UserModel
import com.logient.loreal.security.Configs.AUTHORITIES_KEY
import com.logient.loreal.security.Configs.HEADER_STRING
import com.logient.loreal.security.Configs.SECRET
import com.logient.loreal.security.Configs.TOKEN_PREFIX
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTAuthorizationFilter(authManager: AuthenticationManager) : BasicAuthenticationFilter(authManager) {

    override fun doFilterInternal(req: HttpServletRequest,
                                  res: HttpServletResponse,
                                  chain: FilterChain) {
        val header = req.getHeader(HEADER_STRING)

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res)
            return
        }

        try {
            req.getHeader(HEADER_STRING)?.let { token ->
                // parse the token.
                val jwtDecoded = JWT.require(Algorithm.HMAC512(SECRET.toByteArray()))
                        .build()
                        .verify(token.replace(TOKEN_PREFIX, ""))

                val user = jwtDecoded.subject
                val authorities = ArrayList<GrantedAuthority>()
                val authForToken = jwtDecoded.claims[AUTHORITIES_KEY]?.asArray(String::class.java)
                authForToken?.forEach { auth ->
                    authorities.add(UserModel.Role.byAuthority(auth))
                }
                res.addHeader(HEADER_STRING, TOKEN_PREFIX + JWTAuthenticationFilter.createToken(user, authForToken!!))

                SecurityContextHolder.getContext().authentication = getAuthentication(jwtDecoded)
            }
        } catch (e: TokenExpiredException) {
            logger.error(e)
        }

        chain.doFilter(req, res)
    }

    private fun getAuthentication(jwtDecoded: DecodedJWT): UsernamePasswordAuthenticationToken? {
        val user = jwtDecoded.subject
        val authorities = ArrayList<GrantedAuthority>()
        jwtDecoded.claims[AUTHORITIES_KEY]?.asArray(String::class.java)?.forEach { auth ->
            authorities.add(UserModel.Role.byAuthority(auth))
        }

        return if (user != null) {
            UsernamePasswordAuthenticationToken(user, null, authorities)
        } else null
    }
}