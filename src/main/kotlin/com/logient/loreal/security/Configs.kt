package com.logient.loreal.security

object Configs {
    const val SECRET = "SecretKeyToGenJWTs"
    const val EXPIRATION_TIME = 900000 // 15 minutes
    const val EXPIRATION_TIME_RESET_PASSWORD = 300000 // 5 minutes
    //    const val EXPIRATION_TIME = 1800000 // 30 minutes
    const val TOKEN_PREFIX = "Bearer "
    const val HEADER_STRING = "Authorization"
    const val AUTHORITIES_KEY = "AUTHORITIES_KEY"
}