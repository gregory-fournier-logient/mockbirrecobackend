package com.logient.loreal.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm.HMAC512
import com.fasterxml.jackson.databind.ObjectMapper
import com.logient.loreal.security.Configs.AUTHORITIES_KEY
import com.logient.loreal.security.Configs.EXPIRATION_TIME
import com.logient.loreal.security.Configs.HEADER_STRING
import com.logient.loreal.security.Configs.SECRET
import com.logient.loreal.security.Configs.TOKEN_PREFIX
import com.logient.loreal.security.model.UserLogin
import com.logient.loreal.service.exception.TokenAuthenticationException
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.io.IOException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTAuthenticationFilter(authManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {

    init {
        authenticationManager = authManager
    }

    override fun attemptAuthentication(req: HttpServletRequest, res: HttpServletResponse?): Authentication {
        try {
            val userLogin = ObjectMapper().readValue(req.inputStream, UserLogin::class.java)

            return authenticationManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            userLogin.email,
                            userLogin.password,
                            ArrayList<GrantedAuthority>())
            )
        } catch (e: IOException) {
            throw TokenAuthenticationException("Token authentication exception", e)
        }
    }

    override fun successfulAuthentication(req: HttpServletRequest,
                                          res: HttpServletResponse,
                                          chain: FilterChain?,
                                          auth: Authentication) {
        val user = auth.principal as User
        val authorities: ArrayList<String> = ArrayList()
        user.authorities.forEach {
            authorities.add(it.authority)
        }
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + createToken(user.username, authorities.toTypedArray()))
    }

    companion object {
        fun createToken(username: String, authorities: Array<String>, expirationTime: Int = EXPIRATION_TIME): String {
            return JWT.create()
                    .withSubject(username)
                    .withArrayClaim(AUTHORITIES_KEY, authorities)
                    .withExpiresAt(Date(System.currentTimeMillis() + expirationTime))
                    .sign(HMAC512(SECRET.toByteArray()))
        }
    }
}
