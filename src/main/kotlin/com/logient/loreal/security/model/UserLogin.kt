package com.logient.loreal.security.model

data class UserLogin(var email: String = "", var password: String = "")