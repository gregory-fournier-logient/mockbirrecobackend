package com.logient.loreal.controllers.validators

import javax.validation.Constraint
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [PasswordValidator::class])
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class PasswordConstraint(
        val message: String = "invalid password",
        val groups: Array<KClass<out Any>> = [],
        val payload: Array<KClass<out Any>> = []
)