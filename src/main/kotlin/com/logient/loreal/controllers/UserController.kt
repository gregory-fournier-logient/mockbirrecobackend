package com.logient.loreal.controllers

import com.logient.loreal.security.Configs
import com.logient.loreal.security.JWTAuthenticationFilter
import com.logient.loreal.service.MailService
import com.logient.loreal.service.UserService
import com.logient.loreal.service.model.UserDto
import com.logient.loreal.service.model.ProfileDto
import com.logient.loreal.service.model.RequestPasswordResetDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@RestController
class UserController {

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var mailService: MailService


    @PostMapping(API)
    fun createUser(@RequestBody @Valid user: UserDto, request: HttpServletRequest, response: HttpServletResponse): String {
        //userService.createUser(user, request.getHeader("referer"))
        userService.createUser(user, PUBLIC_URL)
        response.addHeader(Configs.HEADER_STRING, Configs.TOKEN_PREFIX + JWTAuthenticationFilter.createToken(user.email, arrayOf("")))
        return Configs.TOKEN_PREFIX + JWTAuthenticationFilter.createToken(user.email, arrayOf(""))

    }

    @PostMapping(PASSWORD_API)
    fun requestPasswordReset(@RequestBody requestDto: RequestPasswordResetDto) {
        val locale = userService.getProfile(requestDto.email).locale
        mailService.sendResetPasswordLink(requestDto.email, requestDto.url, locale)
    }

    @PutMapping(PASSWORD_API)
    fun resetPassword(@RequestBody profileDto: ProfileDto) {
        val user = SecurityContextHolder.getContext().authentication.principal as String
        userService.changePassword(user, profileDto.passwordNew, profileDto.passwordNewConfirm)
    }

    @PostMapping("$API/authenticateWithBearer")
    fun authenticateWithBearer() {
        SecurityContextHolder.getContext().authentication.principal as String
    }

    // TODO kept as examples, but we don't need them for now
//    @GetMapping(API)
//    @Secured(UserModel.Authority.SUPER_ADMIN)
//    fun getAdmins(): List<UserDto> {
//        return userService.listUsers()
//    }
//
//    @GetMapping("$API/{id}")
//    @Secured(UserModel.Authority.SUPER_ADMIN, UserModel.Authority.ADMIN)
//    fun getUser(@PathVariable id: Long): UserDto {
//        return userService.getUser(id)
//    }
//    @PutMapping("$API/{id}")
//    @Secured(UserModel.Authority.SUPER_ADMIN)
//    fun updateUser(@PathVariable id: Long, @RequestBody @Valid admin: UserDto) {
//        admin.id = id
//        userService.updateUser(admin)
//    }
//
//    @DeleteMapping("$API/{id}/replacement/{replacementId}")
//    @Secured(UserModel.Authority.SUPER_ADMIN)
//    fun deleteUser(@PathVariable("id") id: Long, @PathVariable("replacementId") replacementId: Long) {
//        userService.deleteUser(id, replacementId)
//    }

    companion object {
        const val API = "/api/users"
        const val PUBLIC_URL = "http://d674b21d.ngrok.io"
        const val PASSWORD_API = "/api/resetPassword"
    }
}