package com.logient.loreal.controllers

import com.logient.loreal.controllers.ProductController.Companion.API
import com.logient.loreal.service.model.ProductDto
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ProductController {

    @GetMapping(API)
    fun getProducts(): List<ProductDto> {
        val products = ArrayList<ProductDto>()
        products.add(ProductDto(1, "Budweiser", "https://picsum.photos/id/500/200/200"))
        products.add(ProductDto(2, "Grolsh", "https://picsum.photos/id/501/200/200"))
        products.add(ProductDto(3, "Molson Canadian", "https://picsum.photos/id/502/200/200"))
        products.add(ProductDto(4, "La Fin du Monde", "https://picsum.photos/id/503/200/200"))
        products.add(ProductDto(5, "Péché Mortel", "https://picsum.photos/id/504/200/200"))
        products.add(ProductDto(6, "Saint-Ambroise Pumpkin Ale", "https://picsum.photos/id/505/200/200"))
        products.add(ProductDto(7, "Alexander Keith's Ipa", "https://picsum.photos/id/506/200/200"))
        products.add(ProductDto(8, "CoorsLight", "https://picsum.photos/id/507/200/200"))
        products.add(ProductDto(9, "Labatt Bleue", "https://picsum.photos/id/508/200/200"))
        products.add(ProductDto(10, "Heineken", "https://picsum.photos/id/509/200/200"))
        products.add(ProductDto(11, "Corona", "https://picsum.photos/id/510/200/200"))
        products.add(ProductDto(12, "Brasseur du Monde", "https://picsum.photos/id/511/200/200"))
        return products.toList()
    }

    companion object {
        const val API = "/api/products"
    }
}