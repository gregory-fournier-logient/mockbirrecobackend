package com.logient.loreal.service.exception

class FileStorageException : RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}

class MyFileNotFoundException : RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}

class ValidationException : RuntimeException {
    constructor(message: String) : super(message)
}
class NotFoundException : RuntimeException {
    constructor(message: String) : super(message)
}
class EntityPersistenceException : RuntimeException {
    constructor(message: String) : super(message)
}

class TokenAuthenticationException : RuntimeException {
    constructor(message: String, throwable: Throwable) : super(message, throwable)
}