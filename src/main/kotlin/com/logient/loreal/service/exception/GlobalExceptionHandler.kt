package com.logient.loreal.service.exception

import org.apache.tomcat.util.http.fileupload.FileUploadBase
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.multipart.MaxUploadSizeExceededException
import java.io.IOException
import java.sql.SQLException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@RestControllerAdvice
@ControllerAdvice
class GlobalExceptionHandler {
    var logger: Logger = LoggerFactory.getLogger(GlobalExceptionHandler::class.java)

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "IOException occured")
    @ExceptionHandler(IOException::class)
    fun handleIOException(ex: Exception) {
        logger.error("IOException handler executed", ex)
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "File not found")
    @ExceptionHandler(MyFileNotFoundException::class)
    fun handleFileNotFoundException(ex: Exception) {
        logger.error("FileNotFoundException handler executed", ex)
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException::class, NotFoundException::class, FileStorageException::class)
    fun handleEntityException(ex: Exception, response: HttpServletResponse) {
        if (ex.message.isNullOrEmpty()) {
            logger.error("Incorrect entity handler executed", ex)
            response.sendError(HttpStatus.BAD_REQUEST.value(), "Incorrect entity")
        } else {
            logger.error(ex.message, ex)
            response.sendError(HttpStatus.BAD_REQUEST.value(), ex.message)
        }
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED, reason = "Unauthorized access")
    @ExceptionHandler(RuntimeException::class)
    fun handleUnauthorizedException(ex: RuntimeException) {
        logger.error("Unauthorized handler executed", ex)
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED, reason = "Token authentication error")
    @ExceptionHandler(TokenAuthenticationException::class)
    fun handleTokenAuthenticationException(ex: TokenAuthenticationException) {
        logger.error(ex.message, ex)
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Database exception")
    @ExceptionHandler(SQLException::class)
    fun handleSQLException(ex: SQLException, request: HttpServletRequest) {
        logger.error("SQLException Occured:: URL=" + request.requestURL, ex)
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "File size limit error")
    @ExceptionHandler(FileUploadBase.SizeLimitExceededException::class)
    fun handleSizeLimitExceededException(ex: FileUploadBase.SizeLimitExceededException) {
        logger.error("SizeLimitExceededException handler executed", ex)
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Max upload size limit issue")
    @ExceptionHandler(MaxUploadSizeExceededException::class)
    fun handleMaxUploadSizeExceededException(ex: MaxUploadSizeExceededException) {
        logger.error("MaxUploadSizeExceededException handler executed", ex)
    }

    @ResponseStatus(value = HttpStatus.CONFLICT)
    @ExceptionHandler(EntityPersistenceException::class)
    fun handlePersistenceException(ex: EntityPersistenceException): Map<String, String> {
        logger.error("EntityPersistenceException handler executed", ex)

        return mapOf(Pair("error_message", "Entity not saved"))
    }
}