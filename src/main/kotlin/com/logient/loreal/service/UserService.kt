package com.logient.loreal.service

import com.logient.loreal.service.exception.NotFoundException
import com.logient.loreal.service.exception.ValidationException
import com.logient.loreal.service.model.UserDto
import com.logient.loreal.service.model.ProfileDto

interface UserService {
    fun getProfile(username: String): ProfileDto

    @Throws(ValidationException::class, NotFoundException::class)
    fun updateProfile(profileDto: ProfileDto, username: String, checkPassword: Boolean = true)

    fun getUser(userId: Long): UserDto

    fun listUsers(): List<UserDto>

    @Throws(ValidationException::class)
    fun createUser(userDto: UserDto, requestUrl: String)

    @Throws(ValidationException::class, NotFoundException::class)
    fun updateUser(userDto: UserDto)

    @Throws(NotFoundException::class)
    fun deleteUser(userId: Long, replacementId: Long)

    fun changePassword(email: String, newPassword: String?, newPasswordConfirm: String?)
}