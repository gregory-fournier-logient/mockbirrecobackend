package com.logient.loreal.service



interface MailService {
    fun sendResetPasswordLink(email: String, requestUrl: String, locale: String = "en", isNewUser: Boolean = false)
}