package com.logient.loreal.service.impl

import com.logient.loreal.database.repository.UserRepository
import com.logient.loreal.service.exception.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class UserDetailsServiceImpl : UserDetailsService {

    @Autowired
    private lateinit var userRepository: UserRepository

    override fun loadUserByUsername(username: String): UserDetails {
        val opt = userRepository.findByEmail(username)
        if (!opt.isPresent) {
            throw NotFoundException("UserModel not found")
        }
        val user = opt.get()
        val roles = ArrayList<GrantedAuthority>()
        roles.add(user.getRole())
        return User(user.email, user.password, roles)
    }
}