package com.logient.loreal.service.impl

import com.logient.loreal.database.model.UserModel
import com.logient.loreal.database.repository.LocaleRepository
import com.logient.loreal.database.repository.UserRepository
import com.logient.loreal.service.MailService
import com.logient.loreal.service.UserService
import com.logient.loreal.service.exception.EntityPersistenceException
import com.logient.loreal.service.exception.NotFoundException
import com.logient.loreal.service.exception.ValidationException
import com.logient.loreal.service.model.UserDto
import com.logient.loreal.service.model.ProfileDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Suppress("SpringJavaInjectionPointsAutowiringInspection")
@Service("userDetailsService")
class UserServiceImpl : UserService {
    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var localeRepository: LocaleRepository

    @Autowired
    private lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @Autowired
    private lateinit var mailService: MailService


    //METHODS FOR PROFILES
    override fun getProfile(username: String): ProfileDto {
        val opt = userRepository.findByEmail(username)
        if (!opt.isPresent) {
            throw NotFoundException("UserModel not found")
        }
        return ProfileDto(opt.get())
    }

    override fun updateProfile(profileDto: ProfileDto, username: String, checkPassword: Boolean) {
        if (profileDto.id == null) {
            throw NotFoundException("User not found")
        }
        val opt = userRepository.findById(profileDto.id!!)
        if (!opt.isPresent) {
            throw NotFoundException("User not found")
        }

        val initialUser = opt.get()
        if (initialUser.getRole() == UserModel.Role.CLIENT) {
            throw NotFoundException("User has no company name defined")
        }

        if (profileDto.email != username && initialUser.getRole() !== UserModel.Role.SUPER_ADMIN) {
            throw NotFoundException("Incorrect user")
        }
        if (profileDto.version < opt.get().version) {
            throw EntityPersistenceException("Entity version issue")
        }

        if (profileDto.passwordNew != null) { // user want to change the password
            if (profileDto.passwordNewConfirm == null ||
                    !profileDto.passwordNew.equals(profileDto.passwordNewConfirm)) {
                throw ValidationException("New password not valid")
            }
            if (checkPassword) { // We don't want to check this if user is resetting password
                if (profileDto.passwordOld == null) {
                    throw ValidationException("Old password required")
                }
                if (!bCryptPasswordEncoder.matches(profileDto.passwordOld!!, initialUser.password)) {
                    throw ValidationException("Old password invalid")
                }
            }
            initialUser.password = bCryptPasswordEncoder.encode(profileDto.passwordNew!!)
        }
        fillUserWithProfileDto(initialUser, profileDto)
        userRepository.save(initialUser)
    }

    override fun changePassword(email: String, newPassword: String?, newPasswordConfirm: String?) {
        val profile = getProfile(email)
        profile.passwordNew = newPassword
        profile.passwordNewConfirm = newPassword
        updateProfile(profile, email, false)
    }

    // METHODS FOR ADMINS
    override fun getUser(userId: Long): UserDto {
        val opt = userRepository.findById(userId)
        if (!opt.isPresent) {
            throw NotFoundException("UserModel not found")
        }
        if (!opt.filter { it.getRole() == UserModel.Role.ADMIN }.isPresent) {
            throw NotFoundException("UserModel is not an admin")
        }
        return UserDto(opt.get())
    }

    override fun listUsers(): List<UserDto> {
        return userRepository.findAllByRole(UserModel.Role.ADMIN.name).map { UserDto(it) }
    }

    override fun createUser(userDto: UserDto, requestUrl: String) {
        if (userDto.passwordNew == null) {
            throw ValidationException("New password not valid")
        }
        if (userRepository.countByEmail(userDto.email) > 0) {
            throw ValidationException("UserModel already exist")
        }
        val newUser = UserModel()
        newUser.password = bCryptPasswordEncoder.encode(userDto.passwordNew!!)
        newUser.setRole(UserModel.Role.ADMIN)
        fillUserWithAdminDto(newUser, userDto)
        userRepository.save(newUser)
        mailService.sendResetPasswordLink(userDto.email, requestUrl, userDto.locale, true)
    }

    override fun updateUser(userDto: UserDto) {
        if (userDto.id == null) {
            throw NotFoundException("Admin not found")
        }
        val opt = userRepository.findById(userDto.id!!)
        if (!opt.isPresent) {
            throw NotFoundException("Admin not found")
        }
        if (!opt.filter { it.getRole() == UserModel.Role.ADMIN }.isPresent) {
            throw NotFoundException("UserModel is not an admin")
        }
        if (userDto.version < opt.get().version) {
            throw EntityPersistenceException("Entity version issue")
        }

        val oldUser = opt.get()
        if (userDto.passwordNew != null) { // user want to change the password

            if (userDto.passwordNewConfirm == null ||
                    !userDto.passwordNew.equals(userDto.passwordNewConfirm)) {
                throw ValidationException("New password not valid")
            }
            oldUser.password = bCryptPasswordEncoder.encode(userDto.passwordNew!!)
        }
        fillUserWithAdminDto(oldUser, userDto)
        userRepository.save(oldUser)
    }

    override fun deleteUser(userId: Long, replacementId: Long) {
        if (replacementId > 0L) {
            val opt = userRepository.findById(replacementId)
            if (!opt.isPresent) {
                throw NotFoundException("User not found")
            }
            val replacementAdmin = opt.get()
            if (replacementAdmin.getRole() != UserModel.Role.ADMIN) {
                throw NotFoundException("User is not an admin")
            }
            if (userId == replacementId) {
                throw NotFoundException("Replacement admin must be different")
            }
        }
        userRepository.deleteById(userId)
    }

    //METHODS FOR CLIENTS

    //PRIVATE METHOD TO EASILY FILL MODEL WITH USERS DTO
    private fun fillUserWithProfileDto(userModel: UserModel, profileDto: ProfileDto) {
        userModel.firstName = profileDto.firstName
        userModel.lastName = profileDto.lastName
        userModel.email = profileDto.email
        userModel.locale = localeRepository.findByName(profileDto.locale)
    }

    private fun fillUserWithAdminDto(userModel: UserModel, userDto: UserDto) {
        userModel.firstName = userDto.firstName
        userModel.lastName = userDto.lastName
        userModel.email = userDto.email
        userModel.locale = localeRepository.findByName("en")
    }
}