package com.logient.loreal.service.impl

import com.logient.loreal.database.model.UserModel
import com.logient.loreal.security.Configs
import com.logient.loreal.security.JWTAuthenticationFilter
import com.logient.loreal.service.MailService
import com.logient.loreal.service.UserService
import com.mashape.unirest.http.Unirest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import java.util.*


@Service
class MailServiceImpl : MailService {
    @Value("\${mailgun.key}")
    private lateinit var mailgunKey: String

    @Value("\${mailgun.url}")
    private lateinit var mailgunUrl: String

    @Value("\${mailgun.default.email}")
    private lateinit var defaultEmail: String

    @Autowired
    private lateinit var messageSource: MessageSource

    @Autowired
    var env: Environment? = null

    @Autowired
    private lateinit var userService: UserService

    override fun sendResetPasswordLink(email: String, requestUrl: String, locale: String, isNewUser: Boolean) {
        val user = userService.getProfile(email)
        val role = UserModel.Role.valueOf(user.role).authority
//        val resetLink = requestUrl + RESET_ENDPOINT +
//                JWTAuthenticationFilter.createToken(email, arrayOf(role), Configs.EXPIRATION_TIME_RESET_PASSWORD) + "?locale=${user.locale}"
        val resetLink = "http://bierre.com/" + JWTAuthenticationFilter.createToken(email, arrayOf(role), Configs.EXPIRATION_TIME_RESET_PASSWORD)
        val emailTo = if (env != null && env!!.activeProfiles.contains("prod")) {
            email
        } else {
            defaultEmail
        }
        val subject = if (isNewUser) {
            messageSource.getMessage("email.newUserSubject", arrayOf(user.firstName), Locale(locale))
        } else {
            messageSource.getMessage("email.subject", null, Locale(locale))
        }

        Unirest.post(mailgunUrl)
                .basicAuth("api", mailgunKey)
                .queryString("from", "Logient Info <info@logient.com>")
                .queryString("to", emailTo)
                .queryString("subject", subject)
                .queryString("html", messageSource.getMessage("email.message", arrayOf(resetLink), Locale(locale)))
                .asString()
    }

    companion object {
        const val RESET_ENDPOINT = "#/resetPassword/"
    }
}