package com.logient.loreal.service.model

data class LoginResponse(private val token: String? = null, private val expires: Long? = null, private val username: String? = null)