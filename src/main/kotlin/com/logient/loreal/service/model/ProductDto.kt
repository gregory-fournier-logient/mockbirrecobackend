package com.logient.loreal.service.model

open class ProductDto(
        id: Long,
        val name: String,
        val imageUrl: String
) : AbstractDto() {
    init {
        this.id = id
    }
}