package com.logient.loreal.service.model

import com.logient.loreal.controllers.validators.PasswordConstraint
import com.logient.loreal.database.model.UserModel
import java.util.*
import javax.validation.constraints.NotBlank

class UserDto() : AbstractDto() {

    var firstName: String = ""

    var lastName: String = ""
    @NotBlank
    lateinit var email: String
    var locale: String = "en"

    var birthday: String = ""

    var version:Int = 0

    @PasswordConstraint
    var passwordNew: String? = null

    @PasswordConstraint
    var passwordNewConfirm: String? = null

    constructor(userModel: UserModel) : this() {
        id = userModel.id
        firstName = userModel.firstName
        lastName = userModel.lastName
        email = userModel.email
        version = userModel.version
        locale = userModel.locale.name
    }
}