package com.logient.loreal.service.model

data class RequestPasswordResetDto(val email: String = "", val url: String = "")