package com.logient.loreal.service.model

import com.logient.loreal.controllers.validators.PasswordConstraint
import com.logient.loreal.database.model.UserModel
import javax.validation.constraints.NotBlank

open class ProfileDto() : AbstractDto() {


    var firstName: String = ""

    var lastName: String = ""

    var email: String = ""

    var locale: String = ""

    var role: String = "" //Used only on frontend

    var version = 0
//    var responsibleName: String? = null

    @PasswordConstraint
    var passwordOld: String? = null

    @PasswordConstraint
    var passwordNew: String? = null

    @PasswordConstraint
    var passwordNewConfirm: String? = null

    constructor(userModel: UserModel) : this() {
        id = userModel.id
        firstName = userModel.firstName
        lastName = userModel.lastName
        version = userModel.version
//        responsibleName = userModel.admin?.firstName + " " + userModel.admin?.lastName
        email = userModel.email
        locale = userModel.locale.name
        role = userModel.getRole().name
    }
}