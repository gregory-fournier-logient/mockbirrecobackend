package com.logient.loreal.database.model

import javax.persistence.Column
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class AbstractEnumEntity : AbstractEntity() {

    @Column(name = Fields.NAME, nullable = false)
    lateinit var name: String

    object Fields {
        const val NAME = "name"
    }
}
