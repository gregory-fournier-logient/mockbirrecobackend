package com.logient.loreal.database.model

import javax.persistence.*

@MappedSuperclass
abstract class AbstractEntity(
        @Id
        @Column(name = Fields.ID, nullable = false)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null) {

    object Fields {
        const val ID = "id"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AbstractEntity) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }
}