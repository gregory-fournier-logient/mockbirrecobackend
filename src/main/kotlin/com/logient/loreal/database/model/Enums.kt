package com.logient.loreal.database.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "LOCALES")
class Locale : AbstractEnumEntity() {
    companion object {
        const val FR = "fr"
        const val EN = "en"
    }
}