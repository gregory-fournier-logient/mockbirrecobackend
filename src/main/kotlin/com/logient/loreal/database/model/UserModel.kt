package com.logient.loreal.database.model

import com.logient.loreal.database.repository.PersistenceListener
import org.springframework.security.core.GrantedAuthority
import javax.persistence.*

@Entity
@Table(name = UserModel.TABLE_NAME)
@EntityListeners(PersistenceListener::class)
class UserModel : AbstractModelEntity() {

    @Column(name = Fields.FIRST_NAME, nullable = false)
    lateinit var firstName: String

    @Column(name = Fields.LAST_NAME, nullable = false)
    lateinit var lastName: String

    @Column(name = Fields.EMAIL, nullable = false)
    lateinit var email: String

    @ManyToOne(optional = false)
    @JoinColumn(name = "locale_id")
    lateinit var locale: Locale
//
//    @ManyToOne
//    var admin: UserModel? = null

    @Column(name = Fields.ROLE, nullable = false)
    private lateinit var role: String

    internal fun setRole(role: Role) {
        this.role = role.name
    }

    internal fun getRole(): Role {
        return Role.valueOf(role)
    }

    @Column(name = Fields.PASSWORD, nullable = false)
    lateinit var password: String

    enum class Role(private val authority: String) : GrantedAuthority {
        NONE(Authority.NONE),
        CLIENT(Authority.CLIENT),
        ADMIN(Authority.ADMIN),
        SUPER_ADMIN(Authority.SUPER_ADMIN);

        override fun getAuthority(): String {
            return authority
        }

        companion object {
            fun byAuthority(authority: String): Role {
                for (role in Role.values()) {
                    if (authority == role.authority) {
                        return role
                    }
                }
                return NONE
            }
        }
    }

    companion object {
        const val TABLE_NAME = "USERS"
    }

    //Authorities have to be static/constant in order to be used in SpringSecurity annotation
    object Authority {
        const val NONE = "ROLE_NONE"
        const val CLIENT = "ROLE_CLIENT"
        const val ADMIN = "ROLE_ADMIN"
        const val SUPER_ADMIN = "ROLE_SUPER_ADMIN"
    }

    object Fields {
        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val EMAIL = "email"
        const val PASSWORD = "password"
        const val ROLE = "role"
    }

}