package com.logient.loreal.database.model

import java.util.*
import javax.persistence.Column
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class AbstractModelEntity : AbstractEntity() {
    @Column(name = Fields.CREATED_DATE, nullable = true)
    var createdDate: Date? = null

    @Column(name = Fields.UPDATED_DATE, nullable = true)
    var updatedDate: Date? = null

    @Column(name = Fields.VERSION, nullable = false)
    var version: Int = 0

    object Fields {
        const val CREATED_DATE = "created_date"
        const val UPDATED_DATE = "updated_date"
        const val VERSION = "version"
    }

}
