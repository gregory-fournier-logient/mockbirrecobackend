package com.logient.loreal.database.repository

import com.logient.loreal.database.model.AbstractModelEntity
import com.logient.loreal.service.exception.EntityPersistenceException
import java.util.*
import javax.persistence.PrePersist
import javax.persistence.PreUpdate

class PersistenceListener {
    @PrePersist
    @PreUpdate
    fun methodExecuteBeforeSave(entity: AbstractModelEntity) {
        if (entity.createdDate == null) {
            entity.createdDate = Date()
        }

        if (entity.updatedDate != null &&
                Date().before(entity.updatedDate)) {
            throw  EntityPersistenceException("Update date version issue")
        }
        entity.updatedDate = Date()
        entity.version++
    }
}