package com.logient.loreal.database.repository

import com.logient.loreal.database.model.AbstractEnumEntity
import com.logient.loreal.database.model.Locale
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface LocaleRepository : CrudRepository<Locale, Long> {
    fun findByName(@Param(AbstractEnumEntity.Fields.NAME) name: String): Locale
}