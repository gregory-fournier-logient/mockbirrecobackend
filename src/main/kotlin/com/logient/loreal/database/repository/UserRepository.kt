package com.logient.loreal.database.repository

import com.logient.loreal.database.model.UserModel
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.transaction.annotation.Transactional
import java.util.*

interface UserRepository : CrudRepository<UserModel, Long> {

    fun countByEmail(email: String): Long
//    fun countByAdmin_Id(adminId: Long): Long
    fun findByEmail(email: String): Optional<UserModel>
    fun findAllByRole(role: String): List<UserModel>
//
//    @Modifying
//    @Transactional
//    @Query("UPDATE UserModel user SET user.admin = :replacement WHERE user.admin.id = :adminIdToReplace")
//    fun replaceAdmin(adminIdToReplace: Long, replacement: UserModel)
}