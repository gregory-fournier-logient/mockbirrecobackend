CREATE TABLE LOCALES
(
  id   serial PRIMARY KEY,
  name VARCHAR(20) UNIQUE NOT NULL
);

CREATE TABLE USERS
(
  id           serial PRIMARY KEY,
  version      INTEGER                  NOT NULL DEFAULT 0,
  created_date TIMESTAMP WITH TIME ZONE NULL,
  updated_date TIMESTAMP WITH TIME ZONE NULL,
  first_name   VARCHAR(50)              NOT NULL,
  last_name    VARCHAR(50)              NOT NULL,
  email        VARCHAR(50) UNIQUE       NOT NULL,
  password     VARCHAR(100)             NOT NULL,
  role         VARCHAR(100)             NOT NULL,
  company_name VARCHAR(100)             NULL,
  folder_name  VARCHAR(100)             NULL,
  locale_id    INTEGER                  NOT NULL,
  admin_id     INTEGER                  NULL,

  CONSTRAINT locale_id_fk FOREIGN KEY (locale_id) REFERENCES LOCALES (id),
  CONSTRAINT admin_id_fk FOREIGN KEY (admin_id) REFERENCES USERS (id)
);

CREATE TABLE FILES
(
  id           serial PRIMARY KEY,
  version      INTEGER                  NOT NULL DEFAULT 0,
  created_date TIMESTAMP WITH TIME ZONE NULL,
  updated_date TIMESTAMP WITH TIME ZONE NULL,
  name         VARCHAR(100)             NOT NULL,
  creator      VARCHAR(100)             NOT NULL,
  extension    VARCHAR(20)              NOT NULL,
  is_read      BOOLEAN                  NOT NULL DEFAULT false,
  owner_id     INTEGER                  NOT NULL,
  CONSTRAINT owner_id_fk FOREIGN KEY (owner_id) REFERENCES USERS (id)
);

INSERT INTO LOCALES
VALUES (0, 'fr'),
       (1, 'en');

-- locale = english
-- encrypted password = 'Test123!'
INSERT INTO USERS(first_name,
                  last_name,
                  email,
                  password,
                  role,
                  locale_id)
VALUES ('Jane',
        'Poe',
        'jpoe@example.org',
        '$2a$10$5uRf.NEE9u0J0MlME10ztOUyRTxZvCt7cIt0e77ySLCjEim1QRrxa',
        'SUPER_ADMIN',
        1)