# https://spring.io/guides/topicals/spring-boot-docker

FROM openjdk:8-jdk-alpine as build

WORKDIR /app

COPY gradlew .
COPY gradlew.bat .
COPY gradle gradle
COPY build.gradle .
COPY settings.gradle .
COPY src src

RUN ./gradlew build
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../../build/libs/*.jar)

#########################
FROM openjdk:8-jdk-alpine

RUN apk add su-exec

VOLUME /tmp
ARG DEPENDENCY=/app/target/dependency

COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD java -cp app:app/lib/* \
         -Dspring.profiles.active=dev \
         -Dserver.port=80 \
         com.logient.loreal.LorealApplicationKt
