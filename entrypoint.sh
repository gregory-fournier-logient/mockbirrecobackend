#!/bin/sh

set -e

SHARED_DIR=/usr/uploads

if [[ -d ${SHARED_DIR} ]]; then

  USER_UID=$(stat -c "%u" ${SHARED_DIR})
  USER_GID=$(stat -c "%g" ${SHARED_DIR})

  exec su-exec ${USER_UID}:${USER_GID} "$@"
fi

exec "$@"

